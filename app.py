from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)
app.secret_key = "SECRET_KEY_GOES_HERE"

@app.route("/", methods=['GET'])
def index():
    return render_template("index.html")

if __name__ == '__main__':
    app.run(debug=True, port=5000)
